% function A = sfo_greedy_k_knapsack(F,V,K,W,L,opt)
%   Computes the greedy subset solution of size K for maximizing the
%   submodular function F with knapsack constraint.
%   Implemented by Mario Coutino (mario.coutino@outlook.com) compatible
%   with the SFO library of Andreas Krause (krausea@gmail.com)
%--------------------------------------------------------------------------
% Parameters
% -> F : submodular set-function handler
% -> V : ground set
% -> K : maximum cardinality of solution set
% -> W : weights of elements
% -> L : maximum budget
%--------------------------------------------------------------------------
% Outputs
% <- A : greedy subset solution
%--------------------------------------------------------------------------
function A = sfo_greedy_k_knapsack(F,V,K,W,L,opt)
    
    if ~exist('opt','var')
        opt = sfo_opt;
    end
    
    A = sfo_opt_get(opt,...
        'greedy_initial_sset',[]); %start with empty set or specified
    
    if ~isempty(A)
        V = setdiff(V,A); % removes initial set elements fro ground set
    end

    Acb =  A;
    Auc = A;
    
    Bcb = sum(W(Acb));
    Buc = Bcb;
    
    Vuc = V;
    
    for k = 1:K     % greedy algorithm
        
        N = length(V);
        dFcb = nan(N,1);
        
        for n = 1:N
            if ( ( Bcb + W(V(n)) ) < L )
                dFcb(n) = ( F([Acb V(n)]) - F(Acb) )/W(V(n));
            else
                dFcb(n) = 0;
            end
        end
        
        [~, maxInd] = max(dFcb);
        
        if sfo_opt_get(opt,'verbosity_level',0)>1
            fprintf('Function Value: %f, Cost |A|: %d\n',F(Acb),k);
        end
    
        if ( ( Bcb + W(V(maxInd)) ) < L )
            Acb = [Acb V(maxInd)];
            Bcb = Bcb + W(V(maxInd));
        end
        
        V(maxInd) = [];
        
        % uniform cost solution
        N = length(Vuc);
        
        dFuc = nan(N,1);
        
        for n = 1:N
            if ( ( Buc + W(Vuc(n)) ) < L )
                dFuc(n) = ( F([Auc Vuc(n)]) - F(Auc) );
            else
                dFuc(n) = 0;
            end
        end
        
        [~, maxInd] = max(dFuc);
        
        if sfo_opt_get(opt,'verbosity_level',0)>1
            fprintf('Function Value: %f, Cost |A|: %d\n',F(Auc),k);
        end
    
        if ( ( Buc + W(Vuc(maxInd)) ) < L ) 
            Auc = [Auc Vuc(maxInd)];
            Buc = Buc + W(Vuc(maxInd));
        end
        
        Vuc(maxInd) = [];
    end
    
    if ( F(Auc) > F(Acb) )
        A = Auc;
    else
        A = Acb;
    end
    
end