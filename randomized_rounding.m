function [wsel] = randomized_rounding(w,K,fnc)
    % function [wsel] = randomized_rounding_toeplitz(w,K,fnc)
    % w - non-boolean approximate solution
    % K - number of non zero elements
    % fnc - function to evaluate (handler)

setsize=50;
Wrnd =[];
M= length(w);
 
for count=1:setsize
          
    for m=1:M
            if (rand(1,1) < w(m))
                wrnd(m) = 1;
            else
                wrnd(m)=0;
            end
    end
      
      Wrnd = [Wrnd, wrnd.'];
            
end

Wsel=[];
cost=[];
count=1;

for iter=1:setsize
    
    wtest = Wrnd(:,iter); wtest_idx = find(wtest == 1);
    if (nnz(wtest) == K)
        Wsel = [Wsel wtest];    
        %snr
        snr_aux = fnc(wtest_idx);
        cost = [cost snr_aux];
        count = count+1;
    end
    
end
[~, ind_rnd]=max(cost);
wsel = Wsel(:,ind_rnd);