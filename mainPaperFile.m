%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Paper:
%    Near-Optimal Greedy Sensor Selection for MVDR Beamforming with 
%       Budget Constraints
% Author:
%   Mario Coutino - TUDelft 2017
% Conference:
%   ???????
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear all
close all

M = 50; % number of Sensors
V = 1:M; % ground set

numMeas = 100; % number of measurements

% angle of sources
theta = [-20;-10];
targetId = 1;

f = [0.1;0.4];

% signal-to-noise ratio
SNR = 10;
d = length(theta);
n = 1;

Delta = 0.5; % element spacing in wavelengths
% generate data
[X,A,S,Nn] = gendata(M,numMeas,Delta,theta,f,SNR);

Rxx = 1/numMeas * (X*X');

% interference
Nint = A(:,2:end)*S(2:end,:) + Nn;

% noise + interference covariance matrix
Rnn = 1/numMeas * (Nint*Nint');

targetAngle = theta(targetId);

% steering vector
sVec = gen_a(M,Delta,targetAngle);

a = min(eig(Rnn))/2;
S = Rnn - eye(M);
Sinv = inv(S);
ainv = a^-1;

m = Sinv*sVec;

weights = 10*rand(M,1);
% weights = ones(M,1);
beta = sum(weights)*0.8;
beta = M;

outSNR = @(A)  real(sVec(A)'*inv(Rnn(A,A))*sVec(A));


% submodular surrogate function
fs = @(A) subMSE(Sinv,a,m,A);
Fs = sfo_fn_wrapper(fs);

% function wrapper
G = sfo_fn_wrapper(outSNR);

% budget function
budFnc = @(A) sum(weights(A));
%% Small Size Simulation
wsel_old = zeros(M,1);
kList =  1:1:(M);
Aexh_prev = [];
Acvx_prev = [];
cOutSNR = []; cCost = [];
eOutSNR = []; eCost = [];
sparseOutSNR = []; sparseCost = [];
for kk = kList
    fprintf('Number of Sensors %d \n\n',kk)
%--------------------------------------------------------------------------
% convex relaxation
   fprintf('Convex...\n\n')
    cvx_begin sdp quiet
        variable w(M)
        variable t
        minimize ( t )
        subject to
        sum(w) ==  kk;
            [Sinv + ainv*diag(w), m;
                m' , t] >= semidefinite(M+1);
            w(:)<=1;
            w(:)>=0;
        weights'*w <= beta;
    cvx_end
    
    if isfinite(w)
        flag = 1;
        while flag
            [wsel] = randomized_rounding(w,kk,outSNR);
            if ( (weights'*wsel) <= beta )
                flag = 0;
            end
        end
    else
        wsel = wsel_old;
    end
    
    wsel_old = wsel;
    
    Acvx = find(wsel == 1);
    
    if ( outSNR(Acvx_prev) > outSNR(Acvx) )
        Acvx = Acvx_prev;
    end
    cOutSNR = [cOutSNR; outSNR(Acvx)];
    cCost = [cCost; sum(weights(Acvx))];
      
    fprintf('Sparse Beamforming...\n\n')
    cvx_begin quiet
        variable z(M) complex
        minimize ( z'*Rnn*z )
        subject to
            z'*sVec == 1;
            norm(z,1) <= kk;
    cvx_end
    
    [~,indx] = sort(abs(z),'descend');
    tmpCost = 0;
    zf = []; listLL = [];
    cnt = 1;
    keyboard();
    while ( (length(zf) < kk) & (cnt <= M) )
        pp = indx(cnt);
        if (tmpCost + weights(pp) < beta)
            zf = [zf; z(pp)];
            tmpCost = tmpCost + weights(pp);
            listLL = [listLL; pp];
        end
        if (length(zf) == kk)
            break;
        end
        cnt = cnt + 1;
    end
    disp(length(zf))
    tmp_sparseSNR = 1/abs( zf'*Rnn(listLL,listLL)*zf );
    sparseOutSNR = [sparseOutSNR; tmp_sparseSNR];

% Xcvx = X(Acvx,:);
% Rcvx_nn = Rnn(Acvx,Acvx);
% Rcvx_xx = Rxx(Acvx,Acvx);
% 
% angle = -90:0.1:90;
% dMVDR = nan(angle,1);
% iRcvx = inv(Rcvx_nn);
% 
% z_cvx = (iRcvx*sVec(Acvx))/(sVec(Acvx)'*iRcvx*sVec(Acvx));
% 
% for i = 1 : length(angle)
%     At = gen_a(M,0.5,angle(i));
%     At = At(Acvx,:);
%     dMVDR(i) = abs(z_cvx'*At)^2;
% end
% 
% figure,
% p_cvx = plot(angle,dMVDR,'-g','linewidth',1.1); grid on

%--------------------------------------------------------------------------
% exhaustive search

F = sfo_fn_wrapper(outSNR);

Aexh = sfo_exhaustive_max(F,V,kk,weights,beta);

if isempty(Aexh)
    Aexh = Aexh_prev;
else
    if (F(Aexh_prev) > F(Aexh))
        Aexh = Aexh_prev;
    end
    Aexh_prev = Aexh;
end
fprintf('Exhaustive...\n\n')

eOutSNR = [eOutSNR; outSNR(Aexh)];
eCost = [eCost; sum(weights(Aexh))];
% 
% iRexh = inv(Rnn(Aexh,Aexh));
% z_exh = (iRexh*sVec(Aexh))/(sVec(Aexh)'*iRexh*sVec(Aexh));
% 
% for i = 1 : length(angle)
%     At = gen_a(M,0.5,angle(i));
%     At = At(Aexh,:);
%     dMVDR(i) = abs(z_exh'*At)^2;
% end
% 
% hold on,
% p_exh = plot(angle,dMVDR,'--b','linewidth',1.1);

end

fprintf('Greedy...\n\n')
Agreedy = sfo_greedy_k_knapsack(G,V,M,weights,beta);
gOutSNR = kSetFunctionCost(G,Agreedy'); 
gOutSNR = [gOutSNR; ones(M-size(gOutSNR,1),1)*gOutSNR(end)];
gCost = kSetFunctionCost(budFnc,Agreedy');
gCost = [gCost; ones(M-size(gCost,1),1)*gCost(end)];
% greedy in submodular surrogate
Asub = sfo_greedy_k_knapsack(Fs,V,M,weights,beta);
sOutSNR = kSetFunctionCost(G,Asub');
sOutSNR = [sOutSNR; ones(M-size(sOutSNR,1),1)*sOutSNR(end)];
sCost = kSetFunctionCost(budFnc,Asub');
sCost = [sCost; ones(M-size(sCost,1),1)*sCost(end)];

%% figures
% output snr
maxOutputSNR = outSNR(V);

figure, hold on,
set(gca,'yscale','log','fontsize',12), grid on
p_gred = plot(V, gOutSNR/maxOutputSNR, '-om','linewidth',1.1);
p_sub = plot(V, sOutSNR/maxOutputSNR, '-sk','linewidth',1.1);
p_cvx = plot(kList, cOutSNR/maxOutputSNR, '-^g','linewidth',1.1);
p_sparse = plot(kList, sparseOutSNR/maxOutputSNR, '-^y','linewidth',1.1);
p_exh = plot(kList, eOutSNR/maxOutputSNR, '-dr','linewidth',1.1);
legend([p_cvx; p_sub;p_gred;p_sparse;p_exh],...
    {'Convex Method','Submodular Method',...
    'Output SNR Greedy','Sparse Beamforming','Exhaustive Search'},'location','se')
xlabel('Number of Sensors [k]')
ylabel('Normalized Output SNR')
fig2plotly();
% budget
figure, hold on,
set(gca,'fontsize',12), grid on
p_cvx = plot(kList, cCost, '-^g','linewidth',1.1);
p_gred = plot(V, gCost, '-om','linewidth',1.1);
p_sub = plot(V, sCost, '-sk','linewidth',1.1);
p_exh = plot(kList, eCost, '-dr','linewidth',1.1);
p_con = plot(kList, ones(size(kList))*beta,'--r','linewidth',1.1);
xlabel('Number of Sensors [k]')
ylabel('Budget Consumed')

legend([p_cvx; p_sub;p_gred;p_con;p_exh],...
    {'Convex Method','Submodular Method',...
    'Output SNR Greedy','Budget Constraint','Exhaustive Search'},'location','se')

%%
%--------------------------------------------------------------------------

%% Large Size Simulation

fprintf('Init of Large Size Simulation\n\n')
fprintf('----------------------------------\n\n')
weights = 10*rand(M,1);
% weights = ones(M,1);
% beta = M;
beta = sum(weights)*0.7;

kList =  [1:10:(M) M];
gOutSNR = []; gCost = [];
sOutSNR = []; sCost = [];
wsel_old = zeros(M,1);
budFnc = @(A) sum(weights(A));
%%  greedy - cost benefit heuristic Wolsey et. al 1982
% greedy on original cost function
Agreedy = sfo_greedy_k_knapsack(G,V,M,weights,beta);
gOutSNR = kSetFunctionCost(G,Agreedy'); 
gOutSNR = [gOutSNR; ones(M-size(gOutSNR,1),1)*gOutSNR(end)];
gCost = kSetFunctionCost(budFnc,Agreedy');
gCost = [gCost; ones(M-size(gCost,1),1)*gCost(end)];
% greedy in submodular surrogate
Asub = sfo_greedy_k_knapsack(Fs,V,M,weights,beta);
sOutSNR = kSetFunctionCost(G,Asub');
sOutSNR = [sOutSNR; ones(M-size(sOutSNR,1),1)*sOutSNR(end)];
sCost = kSetFunctionCost(budFnc,Asub');
sCost = [sCost; ones(M-size(sCost,1),1)*sCost(end)];

%%
gOutSNR = []; gCost = [];
sOutSNR = []; sCost = [];
for kk = 1:M
    Agreedy = sfo_greedy_k_knapsack(G,V,kk,weights,beta);
    gOutSNR = [gOutSNR; G(Agreedy)];
    gCost = [gCost; budFnc(Agreedy)];
    
    Asub = sfo_greedy_k_knapsack(Fs,V,kk,weights,beta);
    sOutSNR = [sOutSNR; G(Asub)];
    sCost = [sCost; budFnc(Asub)];
    
end
    
%% convex methods
ll = 1;
angle = -90:0.1:90;
dMVDR_sparse = []; sparseCost = [];
cOutSNR = []; cCost = [];
sparseOutSNR = [];
for kk = kList
    fprintf('Number of Sensors %d \n\n',kk)
    % expensive method - not used in the article... Khuller et al. 2004
%     Agreedy = sfo_knapsack_greedy_max(G,V,weights,beta,kk)';
%     gOutSNR = [gOutSNR; outSNR(Agreedy)];
%     gCost = [gCost; sum(weights(Agreedy))];

    % expensive method
%     Asub = sfo_knapsack_greedy_max(Fs,V,weights,beta,kk)';
%     sOutSNR = [sOutSNR; outSNR(Asub)];
%     sCost = [sCost; sum(weights(Asub))];
    
    fprintf('Convex Init..\n\n')
    cvx_begin sdp quiet
        variable w(M)
        variable t
        minimize ( t )
        subject to
        sum(w) ==  kk;
            [Sinv + ainv*diag(w), m;
                m' , t] >= semidefinite(M+1);
            w(:)<=1;
            w(:)>=0;
        weights'*w <= beta;
    cvx_end
    
    if isfinite(w)
        flag = 1;
        while flag
            [wsel] = randomized_rounding(w,kk,outSNR);
            if ( (weights'*wsel) <= beta )
                flag = 0;
            end
        end
    else
        wsel = wsel_old;
    end
    
    wsel_old = wsel;
    
    Acvx = find(wsel == 1);
    cOutSNR = [cOutSNR; outSNR(Acvx)];
    cCost = [cCost; sum(weights(Acvx))];
    AcvxList{ll} = Acvx;
    ll = ll + 1;
    
    
    fprintf('Convex Sparse Beamformer...\n\n')
    cvx_begin quiet
        variable z(M) complex
        minimize ( z'*Rnn*z )
        subject to
            z'*sVec == 1;
            norm(z,1) <= kk;
    cvx_end
    
    [~,indx] = sort(abs(z),'descend');
    tmpCost = 0;
    zf = []; listLL = [];
    cnt = 1;
    while ( (length(zf) < kk) & (cnt <= M) )
        pp = indx(cnt);
        if (tmpCost + weights(pp) < beta)
            zf = [zf; z(pp)];
            tmpCost = tmpCost + weights(pp);
            listLL = [listLL; pp];
        end
        if (length(zf) == kk)
            break;
        end
        cnt = cnt + 1;
    end
    
    tmp_sparseSNR = 1/abs( zf'*Rnn(listLL,listLL)*zf );
    
    for i = 1 : length(angle)
        At_all = gen_a(M,0.5,angle(i));
        Atsparse = At_all(listLL,:);
        dMVDR_tmp(i) = abs(zf'*Atsparse)^2;
    end
    dMVDR_sparse = [dMVDR_sparse; dMVDR_tmp];
    
    sparseOutSNR = [sparseOutSNR; tmp_sparseSNR];
    sparseCost = [sparseCost; tmpCost];
end
%% figures
% output snr
maxOutputSNR = outSNR(V);

figure, hold on,
set(gca,'yscale','log','fontsize',12), grid on
p_sub = plot(V, sOutSNR/maxOutputSNR, '-sk','linewidth',1.1);
p_cvx = plot(kList, cOutSNR/maxOutputSNR, '-^g','linewidth',1.1);
p_gred = plot(V, gOutSNR/maxOutputSNR, '-om','linewidth',1.1);
p_sparse = plot(kList, abs(sparseOutSNR)/maxOutputSNR, '-^y','linewidth',1.1);
legend([p_cvx; p_sub;p_gred;p_sparse],...
    {'Convex Method','Submodular Method',...
    'Output SNR Greedy','Sparse Beamforming'},'location','se')
xlabel('Number of Sensors [k]')
ylabel('Normalized Output SNR')
fig2plotly();
% budget
figure, hold on,
set(gca,'fontsize',12), grid on
p_sub = plot(V, sCost, '-sk','linewidth',1.1);
p_cvx = plot(kList, cCost, '-^g','linewidth',1.1);
p_gred = plot(V, gCost, '-om','linewidth',1.1);
p_sparse = plot(kList, sparseCost, '-^y','linewidth',1.1);
p_con = plot(V, ones(M,1)*beta,'--r','linewidth',1.1);
xlabel('Number of Sensors [k]')
ylabel('Budget Consumed')

legend([p_cvx; p_sub;p_gred;p_con;p_sparse],...
    {'Convex Method','Submodular Method',...
    'Output SNR Greedy','Budget Constraint','Sparse Beamforming'},'location','se')
fig2plotly();
%%
% 21 sensors
kC = kList(3);
fprintf('Number of Sensors: %f\n\n',kC)
Ag_kC = Agreedy(1:kC);
Asub_kC = Asub(1:kC);
Acvx_kC = AcvxList{3};

angle = -90:0.1:90;
dMVDR_cvx = nan(size(angle));
dMVDR_greedy =nan(size(angle));
dMVDR_subm = nan(size(angle));

bf_mvdr = @(A) (inv(Rnn(A,A))*sVec(A))/(sVec(A)'*inv(Rnn(A,A))*sVec(A));

z_sub = bf_mvdr(Asub_kC);
z_gred = bf_mvdr(Ag_kC);
z_cvx = bf_mvdr(Acvx_kC);

for i = 1 : length(angle)
    At_all = gen_a(M,0.5,angle(i));
    Atcvx = At_all(Acvx_kC,:);
    Atgred = At_all(Ag_kC,:);
    Atsub = At_all(Asub_kC,:);
    dMVDR_cvx(i) = abs(z_cvx'*Atcvx)^2;
    dMVDR_greedy(i) = abs(z_gred'*Atgred)^2;
    dMVDR_subm(i) = abs(z_sub'*Atsub)^2;
end
r_angle = deg2rad(angle);
figure,
%%
trace4 = struct('r',(dMVDR_sparse(3,:)),'t',angle,...
    'mode','lines',...
    'name','Sparse Beamforming',...
    'marker', struct(...
    'color','none',...
    'line',struct('color','yellow')),...
    'type','scatter');
trace3 = struct('r',(dMVDR_greedy),'t',angle,...
    'mode','lines',...
    'name','Convex Method',...
    'marker', struct(...
    'color','none',...
    'line',struct('color','green')),...
    'type','scatter');
trace2 = struct('r',(dMVDR_greedy),'t',angle,...
    'mode','lines',...
    'name','Greedy Output SNR',...
    'marker', struct(...
    'color','none',...
    'line',struct('color','purple')),...
    'type','scatter');
% hold on, %grid on
trace1 = struct('r',(dMVDR_subm),'t',angle,...
    'mode','lines',...
    'name','Submodular Method',...
    'marker', struct(...
    'color','none',...
    'line',struct('color','orangered')),...
    'type','scatter');
layout = struct(...
    'title', 'Beam Patterns', ...
    'font', struct(...
      'family', 'Arial, sans-serif;', ...
      'size', 12, ...
      'color', '#000'), ...
    'showlegend', true, ...
    'width', 500, ...
    'height', 400, ...
    'margin', struct(...
      'l', 40, ...
      'r', 40, ...
      'b', 20, ...
      't', 20, ...
      'pad', 0), ...
    'paper_bgcolor', 'rgb(255, 255, 255)', ...
    'plot_bgcolor', 'rgb(255, 255, 255)', ...
    'orientation', -90,...
    'angularaxis',struct(...
        'tickcolor','rgb(0,0,0)',...
        'range',[0,360])...
    );

response = plotly({trace1,trace2,trace3,trace4}, ...
    struct('layout', layout, 'filename', 'polar-line', 'fileopt', 'overwrite'));
plot_url = response.url
%%
p_sub = polar(r_angle,(dMVDR_subm),'--k'); hold on
p_cvx = polar(r_angle,(dMVDR_cvx),'-g');
p_gred = polar(r_angle,(dMVDR_greedy),'-.m');
xlim([0 1.5])
view([90 -90])
% ylim([0 180])
set([p_sub; p_cvx; p_gred],'linewidth',1.2)
%%
fig = figure;
hold on, grid on
subplot(3,1,1),
p_sub = plot(angle,10*log10(dMVDR_subm),'--k','linewidth',1.1);
xlim([-80,80]),ylim([-40 0]), set(gca,'fontsize',17)
subplot(3,1,2),
p_cvx = plot(angle,10*log10(dMVDR_cvx),'-g','linewidth',1.1);
xlim([-80,80]),ylim([-40 0]), set(gca,'fontsize',17)
ylabel('10log10(||z^{H}a||^{2}) ')
% ylabel('aaaaaaaaaaaaaaaaaaaaaaa ')
subplot(3,1,3),
p_gred = plot(angle,10*log10(dMVDR_greedy),'-.m','linewidth',1.1);
xlim([-80,80]),ylim([-40 0]), set(gca,'fontsize',17)
% subplot(4,1,4),
% p_sparse = plot(angle,10*log10(dMVDR_sparse(3,:)),'-y','linewidth',1.1);
% xlim([-80,80]),ylim([-20 0])
% set(gca,'fontsize',12)
xlabel('DOA Angle [^{o}]')
% xlabel('bbbbbbbbbbbbbbbb')
% set(gca,'yscale','log')
legend([p_cvx; p_sub;p_gred],...
    {'Convex Method','Submodular Method',...
    'Output SNR Greedy'});
% fig2plotly();
%%
% y = randn(10,1);\
theta = angle;
theta_tar = -20;
theta_1 = -90;
theta_2 = 90;
zerodB = 50;
dBYsub = 20*log10(abs(dMVDR_subm)) + zerodB;
dBYcvx = 20*log10(abs(dMVDR_cvx)) + zerodB;
dBYgreedy = 20*log10(abs(dMVDR_greedy)) + zerodB;
dBYsub(find(dBYsub<0))=0;
dBYcvx(find(dBYcvx<0))=0;
dBYgreedy(find(dBYgreedy<0))=0;
% plot(dBYsub.*cos(pi*theta/180), dBYsub.*sin(pi*theta/180), '-', 'LineWidth',2); hold on;
% plot(dBYcvx.*cos(pi*theta/180), dBYcvx.*sin(pi*theta/180), '-r', 'LineWidth',2);
plot(dBYgreedy(1:3:end).*cos(pi*theta(1:3:end)/180), dBYgreedy(1:3:end).*sin(pi*theta(1:3:end)/180), '-g', 'LineWidth',2);
axis([-zerodB zerodB -zerodB zerodB]), axis('off'), axis('square')
hold on
view([90 -90])
plot(zerodB*cos(pi*theta/180),zerodB*sin(pi*theta/180),'k:','LineWidth',1.75) % 0 dB

plot([0 55*cos(theta_1*pi/180)], [0 55*sin(theta_1*pi/180)], 'k:','LineWidth',1.75)
plot([0 55*cos(theta_2*pi/180)], [0 55*sin(theta_2*pi/180)], 'k:','LineWidth',1.75)
hold off

angleTip = [-90:20:90];

for tip = angleTip
    stringText = strcat(num2str(tip),'^{o}');
    text((zerodB+7)*cos(pi*tip/180),(zerodB+7)*sin(pi*tip/180),stringText)
end