% function a = gen_a(M,Delta,theta)
% -------------------------------------------------------------------------
% Generates the array response a(theta) of a uniform linear array with M 
% elements and spacing Delta wavelengths to a source coming from direction
% theta degrees.
% -------------------------------------------------------------------------

function a = gen_a(M,Delta,theta)

    theta = deg2rad(theta);
    phi = exp(1i*2*pi*Delta*real(sin(theta)));
    a = zeros(M,1);
    for i = 0:M-1
        a(i+1) = phi^i;
    end
    
end