% function [X,A,S] = gendata(M,N,Delta,theta,f,SNR)
% -------------------------------------------------------------------------
% Generates a data matrix X = A(theta)S+N as function of the directions 
% Theta = [theta_1 ... theta_d],number of antennas M, number of samples N,  
% and signal-to-noise ratio (SNR) in dB (the SNR is defined as the ratio of 
% the source power of a single user over the noise power). S and N are 
% respectively a dxN and MxN random zero-mean complex Gaussian matrix
% -------------------------------------------------------------------------
function [X,A,S,Nn] = gendata(M,N,Delta,theta,f,SNR)

    d = length(theta);

    k = 0:N-1;
    
    S = exp(1i*2*pi*f*k);
    
    nSigma = sqrt(10^(-SNR/10));
    
    Nn = (nSigma/sqrt(2))*(randn(M,N) + 1i*randn(M,N));
    
    A = zeros(M,d);
    
    for i=1:d
        A(:,i) = gen_a(M,Delta,theta(i));
    end
    
    X = A*S + Nn;

end