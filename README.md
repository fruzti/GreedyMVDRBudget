Near-Optimal Greedy Sensor Selection for MVDR Beamforming with Modular Budget Constraint 

Mario Coutino - S.P Chepuri - G. Leus

Code for the paper "Near-Optimal Greedy Sensor Selection for MVDR Beamforming with Modular Budget Constraint" presented in EUSIPCO 2017.

It requires the SFO Toolbox (http://www.jmlr.org/papers/volume11/krause10a/krause10a.pdf).

https://gitlab.com/fruzti/GreedyMVDRBudget
