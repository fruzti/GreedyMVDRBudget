
nVar = 3;
nPnt = 11;

nCtr = 3;


indx = randperm(nVar);

x = randn(nVar,1);

H = randn(nPnt,nVar);

y = H*x + 3*randn(nPnt,1);

C = randn(nVar,nCtr);

d = C'*x;


H = @(w) H(w,:);

x_ls = @(w) inv(H(w)'*H(w))*H(w)'*y(w);

x_cls = @(w) x_ls(w) + ...
    inv( H(w)'*H(w) ) * C * inv(C'*inv(H(w)'*H(w))*C)*(d - C'*x_ls(w)); 

error = @(w) -norm(x - x_cls(w));



F = sfo_fn_wrapper(error);

V = 1:nPnt;

Ag = sfo_greedy_k(F,V,nPnt);
vG = kSetFunctionCost(F,Ag');

vE = nan(nPnt,1);

for ee = 1:nPnt
    vE(ee) = F(sfo_exhaustive_max(F,V,ee));
end


plot(V, vE, '-ob', 'linewidth', 1.1);
hold on, grid on
plot(V, vG, '--xg', 'linewidth', 1.1);
plot(V, vE*(1-exp(-1)), '--k','linewidth', 1.1)