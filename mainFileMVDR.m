%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Submodular Selection for MVDR
% Mario Coutino
% TUDelft 2016
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
% close all

Delta = 0.5;
M = 11;
N = 100;
theta = [-20;-10];
% theta = round(linspace(-60,60,10));

f = [0.1;0.12];
% f = linspace(-1,1,10)';

% SNR = -5:3:40;
SNR = 10;
d = length(theta);
n = 1;

k= 1; 


[X,A,S,Nn] = gendata(M,N,Delta,theta,f,SNR);
[M,N] = size(X);
Rxx = 1/N * (X*X');

targetAngle = 0;

sVec = gen_a(M,Delta,0);

V =  1:M;

f = @(w) real(trace(inv(Rxx(w,w))*(sVec(w)*sVec(w)')));
g = @(w) real(trace(inv(Rxx(setdiff(V,w),setdiff(V,w)))*...
    (sVec(setdiff(V,w))*sVec(setdiff(V,w))')));

F = sfo_fn_wrapper(f);
G = sfo_fn_wrapper(g);

gFWD = sfo_greedy_k(F,V,M);
gBCK = flipdim(sfo_greedy_k(G,V,M),2);
% gBCK = sfo_greedy_k(G,V,M);

vExh = nan(M,1);
for mm = 1:M
    vExh(mm) = F(sfo_exhaustive_max(F,V,mm));
end

vFWD = kSetFunctionCost(F,gFWD');
vBCK = kSetFunctionCost(F,gBCK');

figure, hold on, grid on
plot(V, vExh, '-ob','linewidth', 1.1)
plot(V, vFWD, '-^m','linewidth', 1.1)
plot(V, vBCK, '-^g','linewidth', 1.1)
plot(V, (1-exp(-1))*vExh, '--r', 'linewidth', 1.1)
legend({'Exhaustive','Greedy FWD', 'Greedy BCK','(1 - 1/e)Bound'})



W = 10*rand(M,1);
L = sum(W)*0.5;

Ak = sfo_knapsack_greedy_max(F,V,W,L);
disp('Greedy Solution Knapsack')
disp(Ak)
fprintf('\n\n Cost: %f/%f\n\n',sum(W(Ak)),L);
fprintf('\n\n Value: %f\n\n', F(Ak))

Aexh = [];
for kk = M:-1:1
    Atmp = sfo_exhaustive_max(F,V,kk,W,L)';
    if ~isempty(Atmp)
        if F(Atmp) > F(Aexh)
            Aexh = Atmp;
        end
    end
end
disp('Exhaustive Search Knapsack')
disp(Aexh)
fprintf('\n\n Cost: %f/%f\n\n',sum(W(Aexh)),L);
fprintf('\n\n Value: %f\n\n', F(Aexh))

fprintf('\n\n Ratio: %f\n\n', F(Ak)/F(Aexh))


[M,N] = size(X);
Xt = X(Ak,:);
Rxx = 1/N * (Xt*Xt');
    
angle = -90:0.1:90;
dMVDR = nan(angle,1);
iRxx = inv(Rxx);
    
for i = 1 : length(angle)
    At = gen_a(M,0.5,angle(i));
    At = At(Ak,:);
    Raa = At*At';
    dMVDR(i) = 1/abs(At'*iRxx*At);
end
% dMVDR = dMVDR/max(dMVDR);    
[~, dFull] = MVDR(X,d);

figure,plot(-90:90,dFull,'-b','linewidth',1.1)
hold on,plot(angle,dMVDR,'--g','linewidth',1.1), grid on
legend('Full MVDR', 'Knapsack Constrained')
