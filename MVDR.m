% function theta = MVDR(X,numAngles)
function [theta, d] = MVDR(X,numAngles)

    [M,N] = size(X);
    Rxx = 1/N * (X*X');
    
    angle = -90:90;
    d = size(angle);
    
    iRxx = inv(Rxx);
    
    for i = 1 : length(angle)
        At = gen_a(M,0.5,angle(i));
        Raa = At*At';
        d(i) = 1/abs(At'*iRxx*At);
    end
    
%     d = d/max(d);
    
    [pks,indx] = findpeaks(d);
    
    [~,i] = sort(pks);
    
%     theta = angle(indx);
%     
%     if length(i) >= numAngles
%         theta = sort(angle(indx(i(end-numAngles+1:end))));
%     else
%         theta = [angle(indx(i)) 0];
%     end

    if length(i) >= numAngles
        theta = sort(angle(indx(i(end-numAngles+1:end))));
    else
%         theta = [angle(indx(i))];
        l = length(i);
        theta = [angle(indx(i)) ones(1,numAngles-l)*NaN];
    end
end