% function A = sfo_greedy_k_knapsack(F,V,K,W,L,opt)
%   Computes the greedy subset solution of size K for maximizing the
%   submodular function F with knapsack constraint.
%   Implemented by Mario Coutino (mario.coutino@outlook.com) compatible
%   with the SFO library of Andreas Krause (krausea@gmail.com)
%--------------------------------------------------------------------------
% Parameters
% -> F : submodular set-function handler
% -> V : ground set
% -> K : maximum cardinality of solution set
% -> W : weights of elements
% -> L : maximum budget
%--------------------------------------------------------------------------
% Outputs
% <- A : greedy subset solution
%--------------------------------------------------------------------------
function A = sfo_greedy_k(F,V,K,opt)
    
    if ~exist('opt','var')
        opt = sfo_opt;
    end
    
    A = sfo_opt_get(opt,...
        'greedy_initial_sset',[]); %start with empty set or specified
    
    if ~isempty(A)
        V = setdiff(V,A); % removes initial set elements fro ground set
    end

    Bo = sum(W(A));
    
    for k = 1:K     % greedy algorithm
        
        N = length(V);
        dF = nan(N,1);
        
        for n = 1:N
            if ( ( Bo + W(V(n)) ) < L )
                dF(n) = ( F([A V(n)]) - F(A) )/W(V(n));
            else
                dF(n) = 0;
            end
        end
        
        [~, maxInd] = max(dF);
        
        if sfo_opt_get(opt,'verbosity_level',0)>1
            fprintf('Function Value: %f, Cost |A|: %d\n',F(A),k);
        end
    
        A = [A V(maxInd)];
        Bo = Bo + W(V(maxInd));
        
        V(maxInd) = [];
    end
    
end