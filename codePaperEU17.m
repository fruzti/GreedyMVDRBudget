%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Paper:
%    Near-Optimal Greedy Sensor Selection for MVDR Beamforming with 
%       Budget Constraints
% Author:
%   Mario Coutino - TUDelft 2017
% Conference:
%   EUROSIPCO 2017
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear all
close all

M = 50; % number of Sensors
V = 1:M; % ground set

numMeas = 100; % number of measurements

% angle of sources
theta = [-20;-10];
targetId = 1;

f = [0.1;0.4];

% signal-to-noise ratio
SNR = 10;
d = length(theta);
n = 1;

Delta = 0.5; % element spacing in wavelengths
% generate data
[X,A,S,Nn] = gendata(M,numMeas,Delta,theta,f,SNR);

Rxx = 1/numMeas * (X*X');

% interference
Nint = A(:,2:end)*S(2:end,:) + Nn;

% noise + interference covariance matrix
Rnn = 1/numMeas * (Nint*Nint');

targetAngle = theta(targetId);

% steering vector
sVec = gen_a(M,Delta,targetAngle);

a = min(eig(Rnn))/2;
S = Rnn - eye(M);
Sinv = inv(S);
ainv = a^-1;

m = Sinv*sVec;

weights = 10*rand(M,1);
% weights = ones(M,1);
beta = sum(weights)*0.8;
beta = M;

outSNR = @(A)  real(sVec(A)'*inv(Rnn(A,A))*sVec(A));


% submodular surrogate function
fs = @(A) subMSE(Sinv,a,m,A);
Fs = sfo_fn_wrapper(fs);

% function wrapper
G = sfo_fn_wrapper(outSNR);

% budget function
budFnc = @(A) sum(weights(A));

%% Small Size Simulation
wsel_old = zeros(M,1);
kList =  1:1:(M);
Aexh_prev = [];
Acvx_prev = [];
cOutSNR = []; cCost = [];
eOutSNR = []; eCost = [];
sparseOutSNR = []; sparseCost = [];
for kk = kList
    fprintf('Number of Sensors %d \n\n',kk)
%--------------------------------------------------------------------------
% convex relaxation
   fprintf('Convex...\n\n')
    cvx_begin sdp quiet
        variable w(M)
        variable t
        minimize ( t )
        subject to
        sum(w) ==  kk;
            [Sinv + ainv*diag(w), m;
                m' , t] >= semidefinite(M+1);
            w(:)<=1;
            w(:)>=0;
        weights'*w <= beta;
    cvx_end
    
    if isfinite(w)
        flag = 1;
        while flag
            [wsel] = randomized_rounding(w,kk,outSNR);
            if ( (weights'*wsel) <= beta )
                flag = 0;
            end
        end
    else
        wsel = wsel_old;
    end
    
    wsel_old = wsel;
    
    Acvx = find(wsel == 1);
    
    if ( outSNR(Acvx_prev) > outSNR(Acvx) )
        Acvx = Acvx_prev;
    end
    cOutSNR = [cOutSNR; outSNR(Acvx)];
    cCost = [cCost; sum(weights(Acvx))];
%--------------------------------------------------------------------------
% sparse beamformer
    fprintf('Sparse Beamforming...\n\n')
    cvx_begin quiet
        variable z(M) complex
        minimize ( z'*Rnn*z )
        subject to
            z'*sVec == 1;
            norm(z,1) <= kk;
    cvx_end
    
    [~,indx] = sort(abs(z),'descend');
    tmpCost = 0;
    zf = []; listLL = [];
    cnt = 1;
    while ( (length(zf) < kk) & (cnt <= M) )
        pp = indx(cnt);
        if (tmpCost + weights(pp) < beta)
            zf = [zf; z(pp)];
            tmpCost = tmpCost + weights(pp);
            listLL = [listLL; pp];
        end
        if (length(zf) == kk)
            break;
        end
        cnt = cnt + 1;
    end
    disp(length(zf))
    tmp_sparseSNR = 1/abs( zf'*Rnn(listLL,listLL)*zf );
    sparseOutSNR = [sparseOutSNR; tmp_sparseSNR];
%--------------------------------------------------------------------------
% exhaustive search

F = sfo_fn_wrapper(outSNR);

Aexh = sfo_exhaustive_max(F,V,kk,weights,beta);

if isempty(Aexh)
    Aexh = Aexh_prev;
else
    if (F(Aexh_prev) > F(Aexh))
        Aexh = Aexh_prev;
    end
    Aexh_prev = Aexh;
end
fprintf('Exhaustive...\n\n')

eOutSNR = [eOutSNR; outSNR(Aexh)];
eCost = [eCost; sum(weights(Aexh))];
end
%--------------------------------------------------------------------------
% Greedy Methods
fprintf('Greedy...\n\n')
Agreedy = sfo_greedy_k_knapsack(G,V,M,weights,beta);
gOutSNR = kSetFunctionCost(G,Agreedy'); 
gOutSNR = [gOutSNR; ones(M-size(gOutSNR,1),1)*gOutSNR(end)];
gCost = kSetFunctionCost(budFnc,Agreedy');
gCost = [gCost; ones(M-size(gCost,1),1)*gCost(end)];
% greedy in submodular surrogate
Asub = sfo_greedy_k_knapsack(Fs,V,M,weights,beta);
sOutSNR = kSetFunctionCost(G,Asub');
sOutSNR = [sOutSNR; ones(M-size(sOutSNR,1),1)*sOutSNR(end)];
sCost = kSetFunctionCost(budFnc,Asub');
sCost = [sCost; ones(M-size(sCost,1),1)*sCost(end)];
%% figures
maxOutputSNR = outSNR(V);

figure, hold on,
set(gca,'yscale','log','fontsize',12), grid on
p_gred = plot(V, gOutSNR/maxOutputSNR, '-om','linewidth',1.1);
p_sub = plot(V, sOutSNR/maxOutputSNR, '-sk','linewidth',1.1);
p_cvx = plot(kList, cOutSNR/maxOutputSNR, '-^g','linewidth',1.1);
% p_sparse = plot(kList, sparseOutSNR/maxOutputSNR, '-^y','linewidth',1.1);
p_exh = plot(kList, eOutSNR/maxOutputSNR, '-dr','linewidth',1.1);
legend([p_cvx; p_sub;p_gred;p_exh],...
    {'Convex Method','Submodular Method',...
    'Output SNR Greedy','Exhaustive Search'},...
    'location','se')
xlabel('Number of Sensors [k]')
ylabel('Normalized Output SNR')
%fig2plotly();

figure, hold on,
set(gca,'fontsize',12), grid on
p_cvx = plot(kList, cCost, '-^g','linewidth',1.1);
p_gred = plot(V, gCost, '-om','linewidth',1.1);
p_sub = plot(V, sCost, '-sk','linewidth',1.1);
p_exh = plot(kList, eCost, '-dr','linewidth',1.1);
p_con = plot(kList, ones(size(kList))*beta,'--r','linewidth',1.1);
xlabel('Number of Sensors [k]')
ylabel('Budget Consumed')

legend([p_cvx; p_sub;p_gred;p_con;p_exh],...
    {'Convex Method','Submodular Method',...
    'Output SNR Greedy','Budget Constraint','Exhaustive Search'},...
    'location','se')
%% Large Size Simulation (run first cell w/ M = 50)

fprintf('Init of Large Size Simulation\n\n')
fprintf('----------------------------------\n\n')
weights = 10*rand(M,1);
% weights = ones(M,1);
% beta = M;
beta = sum(weights)*0.7;

kList =  [1:10:(M) M];
gOutSNR = []; gCost = [];
sOutSNR = []; sCost = [];
wsel_old = zeros(M,1);
budFnc = @(A) sum(weights(A));
%--------------------------------------------------------------------------
%  greedy - cost benefit heuristic Wolsey et. al 1982

% greedy on original cost function
Agreedy = sfo_greedy_k_knapsack(G,V,M,weights,beta);
gOutSNR = kSetFunctionCost(G,Agreedy'); 
gOutSNR = [gOutSNR; ones(M-size(gOutSNR,1),1)*gOutSNR(end)];
gCost = kSetFunctionCost(budFnc,Agreedy');
gCost = [gCost; ones(M-size(gCost,1),1)*gCost(end)];
% greedy in submodular surrogate
Asub = sfo_greedy_k_knapsack(Fs,V,M,weights,beta);
sOutSNR = kSetFunctionCost(G,Asub');
sOutSNR = [sOutSNR; ones(M-size(sOutSNR,1),1)*sOutSNR(end)];
sCost = kSetFunctionCost(budFnc,Asub');
sCost = [sCost; ones(M-size(sCost,1),1)*sCost(end)];
%--------------------------------------------------------------------------
% convex methods
ll = 1;
angle = -90:0.1:90;
dMVDR_sparse = []; sparseCost = [];
cOutSNR = []; cCost = [];
sparseOutSNR = [];
for kk = kList
    fprintf('Number of Sensors %d \n\n',kk)
    % expensive method - not used in the article... Khuller et al. 2004
%     Agreedy = sfo_knapsack_greedy_max(G,V,weights,beta,kk)';
%     gOutSNR = [gOutSNR; outSNR(Agreedy)];
%     gCost = [gCost; sum(weights(Agreedy))];

    % expensive method
%     Asub = sfo_knapsack_greedy_max(Fs,V,weights,beta,kk)';
%     sOutSNR = [sOutSNR; outSNR(Asub)];
%     sCost = [sCost; sum(weights(Asub))];
%--------------------------------------------------------------------------
% Convex Method
    fprintf('Convex Init..\n\n')
    cvx_begin sdp quiet
        variable w(M)
        variable t
        minimize ( t )
        subject to
        sum(w) ==  kk;
            [Sinv + ainv*diag(w), m;
                m' , t] >= semidefinite(M+1);
            w(:)<=1;
            w(:)>=0;
        weights'*w <= beta;
    cvx_end
    
    if isfinite(w)
        flag = 1;
        while flag
            [wsel] = randomized_rounding(w,kk,outSNR);
            if ( (weights'*wsel) <= beta )
                flag = 0;
            end
        end
    else
        wsel = wsel_old;
    end
    
    wsel_old = wsel;
    
    Acvx = find(wsel == 1);
    cOutSNR = [cOutSNR; outSNR(Acvx)];
    cCost = [cCost; sum(weights(Acvx))];
    AcvxList{ll} = Acvx;
    ll = ll + 1;
    
%--------------------------------------------------------------------------
% Sparse Beamformer
    fprintf('Convex Sparse Beamformer...\n\n')
    cvx_begin quiet
        variable z(M) complex
        minimize ( z'*Rnn*z )
        subject to
            z'*sVec == 1;
            norm(z,1) <= kk;
    cvx_end
    
    [~,indx] = sort(abs(z),'descend');
    tmpCost = 0;
    zf = []; listLL = [];
    cnt = 1;
    while ( (length(zf) < kk) & (cnt <= M) )
        pp = indx(cnt);
        if (tmpCost + weights(pp) < beta)
            zf = [zf; z(pp)];
            tmpCost = tmpCost + weights(pp);
            listLL = [listLL; pp];
        end
        if (length(zf) == kk)
            break;
        end
        cnt = cnt + 1;
    end
    
    tmp_sparseSNR = 1/abs( zf'*Rnn(listLL,listLL)*zf );
    
    for i = 1 : length(angle)
        At_all = gen_a(M,0.5,angle(i));
        Atsparse = At_all(listLL,:);
        dMVDR_tmp(i) = abs(zf'*Atsparse)^2;
    end
    dMVDR_sparse = [dMVDR_sparse; dMVDR_tmp];
    
    sparseOutSNR = [sparseOutSNR; tmp_sparseSNR];
    sparseCost = [sparseCost; tmpCost];
end
%% figures
% output snr
maxOutputSNR = outSNR(V);

figure, hold on,
set(gca,'yscale','log','fontsize',12), grid on
p_sub = plot(V, sOutSNR/maxOutputSNR, '-sk','linewidth',1.1);
p_cvx = plot(kList, cOutSNR/maxOutputSNR, '-^g','linewidth',1.1);
p_gred = plot(V, gOutSNR/maxOutputSNR, '-om','linewidth',1.1);
p_sparse = plot(kList, abs(sparseOutSNR)/maxOutputSNR, '-^y','linewidth',1.1);
legend([p_cvx; p_sub;p_gred;p_sparse],...
    {'Convex Method','Submodular Method',...
    'Output SNR Greedy','Sparse Beamforming'},'location','se')
xlabel('Number of Sensors [k]')
ylabel('Normalized Output SNR')
fig2plotly();
% budget
figure, hold on,
set(gca,'fontsize',12), grid on
p_sub = plot(V, sCost, '-sk','linewidth',1.1);
p_cvx = plot(kList, cCost, '-^g','linewidth',1.1);
p_gred = plot(V, gCost, '-om','linewidth',1.1);
p_sparse = plot(kList, sparseCost, '-^y','linewidth',1.1);
p_con = plot(V, ones(M,1)*beta,'--r','linewidth',1.1);
xlabel('Number of Sensors [k]')
ylabel('Budget Consumed')

legend([p_cvx; p_sub;p_gred;p_con;p_sparse],...
    {'Convex Method','Submodular Method',...
    'Output SNR Greedy','Budget Constraint','Sparse Beamforming'},'location','se')

%% figure - beamformer pattern
% 21 sensors
kC = kList(3);
fprintf('Number of Sensors: %f\n\n',kC)
Ag_kC = Agreedy(1:kC);
Asub_kC = Asub(1:kC);
Acvx_kC = AcvxList{3};

angle = -90:0.1:90;
dMVDR_cvx = nan(size(angle));
dMVDR_greedy =nan(size(angle));
dMVDR_subm = nan(size(angle));

bf_mvdr = @(A) (inv(Rnn(A,A))*sVec(A))/(sVec(A)'*inv(Rnn(A,A))*sVec(A));

z_sub = bf_mvdr(Asub_kC);
z_gred = bf_mvdr(Ag_kC);
z_cvx = bf_mvdr(Acvx_kC);

for i = 1 : length(angle)
    At_all = gen_a(M,0.5,angle(i));
    Atcvx = At_all(Acvx_kC,:);
    Atgred = At_all(Ag_kC,:);
    Atsub = At_all(Asub_kC,:);
    dMVDR_cvx(i) = abs(z_cvx'*Atcvx)^2;
    dMVDR_greedy(i) = abs(z_gred'*Atgred)^2;
    dMVDR_subm(i) = abs(z_sub'*Atsub)^2;
end
fig = figure;
hold on, grid on
subplot(3,1,1),
p_sub = plot(angle,10*log10(dMVDR_subm),'--k','linewidth',1.1);
xlim([-80,80]),ylim([-40 0]), set(gca,'fontsize',17)
subplot(3,1,2),
p_cvx = plot(angle,10*log10(dMVDR_cvx),'-g','linewidth',1.1);
xlim([-80,80]),ylim([-40 0]), set(gca,'fontsize',17)
ylabel('10log10(||z^{H}a||^{2}) ')
subplot(3,1,3),
p_gred = plot(angle,10*log10(dMVDR_greedy),'-.m','linewidth',1.1);
xlim([-80,80]),ylim([-40 0]), set(gca,'fontsize',17)
xlabel('DOA Angle [^{o}]')
legend([p_cvx; p_sub;p_gred],...
    {'Convex Method','Submodular Method',...
    'Output SNR Greedy'});