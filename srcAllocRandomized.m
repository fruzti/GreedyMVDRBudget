

clear all,

nPj = 5;
nStd = 30;
nOpt = 3;

% Matrix with student options
A = nan(nStd,nOpt);

for i = 1:nStd
    A(i,:) = randperm(nPj,nOpt);
end

fVec = nan(nPj,1);

for i = 1:nPj
    fVec(i) = sum(A(:) == i);
end

fMin = ceil(min(fVec)/nPj) + 1;

% Assignation of student projects
pS = nan(nStd,1);

f = zeros(nPj,1);

for i = 1:nStd
    
    flag = 0;
    k = 1;
    
    while ~flag
        a = A(i,k);
    
        if f(a) <= fMin
            pS(i) = a;
            flag = 1;
            f(a) = f(a) + 1;
        else
            k = k + 1;
        end
        
        if k > 3
            l = randperm(3,1);
            a = A(i,l);
            pS(i) = a;
            f(a) = f(a) + 1;
            flag = 1;
        end
    end
end

% Distribution of student projects
hist(pS)
fprintf('\n\nRatio of 1st Choice: %f\n\n',100*sum(pS == A(:,1))/nStd)
fprintf('\n\nRatio of 2nd Choice: %f\n\n',100*sum(pS == A(:,2))/nStd)
fprintf('\n\nRatio of 3nd Choice: %f\n\n',100*sum(pS == A(:,3))/nStd)