nVar = 15;
nPnt = 20;

nCtr = 3;
nIt = 1;
ratio = nan(nVar,1);
ratioB = nan(nVar,1);

for ii = 1:nIt
    
indx = randperm(nVar);

x = randn(nVar,1);

H = randn(nPnt,nVar);

y = H*x + 3*randn(nPnt,1);

C = randn(nVar,nCtr);

d = C'*x;


H = @(w) H(:,w);
C = @(w) C(w,:);

x_ls = @(w) inv(H(w)'*H(w))*H(w)'*y;

x_cls = @(w) x_ls(w) + ...
    inv( H(w)'*H(w) ) * C(w) * inv(C(w)'*inv(H(w)'*H(w))*C(w))*(d - C(w)'*x_ls(w)); 

error = @(w) -norm(y - H(w)*x_cls(w));



F = sfo_fn_wrapper(error);
G = sfo_fn_invert(F,V);

V = 1:nVar;

Ag = sfo_greedy_k(F,V,nVar);
vG = kSetFunctionCost(F,Ag');

Ab = flipdim(sfo_greedy_k(G,V,nVar),2);
vB = kSetFunctionCost(F,Ab');

vE = nan(nVar,1);

for ee = 1:nVar
    vE(ee) = F(sfo_exhaustive_max(F,V,ee));
end

ratio(:,ii) = vG./vE;
ratioB(:,ii) = vB./vE;

end

plot(V(nCtr:end), -vG(nCtr:end), '-^g', 'linewidth', 1.1), hold on, grid on,
plot(V(nCtr:end), -vB(nCtr:end), '-om', 'linewidth', 1.1),
plot(V(nCtr:end), -vE(nCtr:end), '-b', 'linewidth', 1.1)
plot(V(nCtr:end), -(vE(nCtr:end)*(1+exp(-1))), '--k', 'linewidth', 1.1)
%%
mRatio = nanmean(ratio,2);
mRatioB = nanmean(ratioB,2);

figure, set(gca,'fontSize',14)
plot(V(nCtr:end), mRatio(nCtr:end),'-^g' ,'linewidth', 1.1), hold on, grid on,
plot(V(nCtr:end), mRatioB(nCtr:end),'-om','linewidth', 1.1),hold on
plot(V(nCtr:end), (1-exp(-1))^-1*ones(nVar-nCtr + 1,1),'--k', 'linewidth', 1.1);
legend({'FWD Greedy Ratio','BCK Greedy Ratio','(1-1/e) Bound'})