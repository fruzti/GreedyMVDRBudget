function Ak = sfo_knapsack_greedy_max(F,V,W,L,K)

    if nargin < 5
        K = length(V);
    end
    
    % First Phase
    Sk1 = sfo_exhaustive_max(F,V,1,W,L);
    if K == 1
        Ak = Sk1;
        return
    end
    
    Sk2 = sfo_exhaustive_max(F,V,2,W,L);
    if K == 2
        Ak = Sk2;
        return
    end
    
    Stmp = {Sk1,Sk2};
    [vS1, indx] = max([F(Sk1'), F(Sk2')]);
    
    S1 = Stmp{indx};
    
    % Second Phase
    tstVec = combnk(V,3);
    nComb = size(tstVec,1);
    
    valF = nan(nComb,1);
    costS = nan(nComb,1);
    
    for c = 1:nComb
        v = tstVec(c,:);
        valF(c) = F(v);
        costS(c) = sum(W(v));
    end

    feasibleSets = tstVec(costS <= L, :);
    costS = costS(costS <= L);
    
    nSets = size(feasibleSets,1);
    
    S2 = [];
    vS2 = F(S2);
    
    for nn = 1:nSets
        
        Vg = V;
        
        A = feasibleSets(nn,:);

        Vg = setdiff(Vg,A); % removes initial set elements from ground set
        
        tmpCost = costS(nn);
        
        for k = 1:(K - length(A))     % greedy algorithm w/ knapsack
        
            N = length(Vg);
            dF = nan(N,1);
        
            for n = 1:N
                dF(n) = ( F([A Vg(n)]) - F(A) ) / W(Vg(n));
            end
        
            [~, maxInd] = max(dF);
            if ( tmpCost + W(Vg(maxInd)) ) <= L
                A = [A Vg(maxInd)];
                tmpCost = tmpCost + W(Vg(maxInd));
            end
            Vg(maxInd) = [];
        end
        
        if ( F(A) > F(S2) )
            S2 = A;
            vS2 = F(A);
        end
    end
    
    if vS1 > vS2
        Ak = S1;
    else
        Ak = S2;
    end
    
end